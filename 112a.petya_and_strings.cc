#include <bits/stdc++.h>
using namespace std;

int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    string foo, bar;
    cin >> foo >> bar;
    transform(begin(foo), end(foo), begin(foo), ::tolower);
    transform(begin(bar), end(bar), begin(bar), ::tolower);
    int lexorder = foo.compare(bar);
    if (lexorder == 0)
        cout << 0 << endl;
    else if (lexorder < 0)
        cout << -1 << endl;
    else
        cout << 1 << endl;
    return 0;
}
