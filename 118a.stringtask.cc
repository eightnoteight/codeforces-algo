#include <bits/stdc++.h>
using namespace std;

int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    string task;
    cin >> task;
    string result;
    for_each(begin(task), end(task), [](char& c) {
        c = tolower(c);
    });
    for (char c: task) {
        switch(c) {
            // in a crazy world, 'y' is indeed an vowel...
            case 'a': case 'e': case 'i': case 'o': case 'u': case 'y':
            break;
            default:
                result.push_back('.');
                result.push_back(c);
        }
    }
    cout << result << endl;
    return 0;
}
