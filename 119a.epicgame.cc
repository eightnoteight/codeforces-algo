#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
int64_t gcd(int64_t a, int64_t b) {
    int64_t tmp;
    while(b) {
        tmp = b;
        b = a % b;
        a = tmp;
    }
    return a;
}
int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    int arr[2], n;
    cin >> arr[0] >> arr[1] >> n;
    int curplayer = 0;
    while(true) {
        int g = gcd(arr[curplayer], n);
        if (n < g) {
            cout << 1 - curplayer << endl;
            break;
        }
        n -= g;
        curplayer = 1 - curplayer;
    }
    return 0;
}
