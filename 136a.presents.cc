#include <bits/stdc++.h>
using namespace std;

int main() {
    int n;
    cin >> n;
    int arr[n + 1];
    for (int x = 1; x <= n; ++x) {
        int _x;
        cin >> _x;
        arr[_x] = x;
    }
    for (int x = 1; x <= n; ++x)
        cout << arr[x] << ' ';
    return 0;
}
