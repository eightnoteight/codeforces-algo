#include <bits/stdc++.h>
using namespace std;

int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    int n, sum = 0;
    cin >> n;
    vector<int> arr(n);
    for (int x = 0; x < n; ++x) {
        cin >> arr[x];
        sum += arr[x];
    }
    sort(arr.begin(), arr.end(), greater<int>());
    int items = 0, cursum = 0;
    for(int x: arr) {
        items++;
        cursum += x;
        sum -= x;
        if (cursum > sum)
            break;
    }
    cout << items << endl;
    return 0;
}
