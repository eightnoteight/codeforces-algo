#include <bits/stdc++.h>
using namespace std;

int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    int nvisitors;
    cin >> nvisitors;
    auto lastCustomer = make_pair(-1, -1);
    int cur = 0, ans = 0;
    while(nvisitors--) {
        pair<int, int> customer;
        cin >> customer.first >> customer.second;
        if (lastCustomer == customer)
            cur++;
        else
            cur = 1;
        lastCustomer = customer;
        ans = max(ans, cur);
    }
    cout << ans << endl;
    return 0;
}
