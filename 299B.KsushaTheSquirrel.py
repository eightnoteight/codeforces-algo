#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin

def main():
    n, k = map(int, raw_input().split())
    road = raw_input().strip()
    dots = 0
    for x in xrange(n - 1, -1, -1):
        if road[x] == '.':
            dots += 1
        if n - x > k and road[x + k] == '.':
            dots -= 1
        if dots <= 0:
            print 'NO'
            return
    print 'YES'

main()
