#include <bits/stdc++.h>
using namespace std;

int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    string notes;
    cin >> notes;
    int counts[] = {0, 0, 0, 0};
    for(char c: notes)
        if (c == '1' || c == '2' || c == '3')
            counts[c - '0']++;
    string xenianotes;
    char c = '0';
    for (int x = 1; x <= 3; ++x) {
        while(counts[x]--) {
            xenianotes.push_back('0' + x);
            xenianotes.push_back('+');
        }
    }
    xenianotes.pop_back();
    cout << xenianotes << endl;
    return 0;
}
