n = int(raw_input())
v = map(int, raw_input().split())
vsums = [0]*(n + 1)
for x in xrange(1, n + 1):
    vsums[x] += vsums[x - 1] + v[x - 1]
u = sorted(v)
usums = [0]*(n + 1)
for x in xrange(1, n + 1):
    usums[x] += usums[x - 1] + u[x - 1]
m = int(raw_input())
for x in xrange(m):
    t, l, r = map(int, raw_input().split())
    if t == 1:
        print vsums[r] - vsums[l - 1]
    else:
        print usums[r] - usums[l - 1]
