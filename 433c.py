def min_pages(n, m, a):
    if m == 1:
        return 0
    elif m == 2:
        return abs(a[1] - a[0])
    maxshift = abs(a[1] - a[0])
    index = 0
    for x in xrange(1, m - 1):
        shift = (abs(a[x + 1] - a[x]) +
                 abs(a[x] - a[x - 1]) -
                 abs(a[x + 1] - a[x - 1]))
        if shift > maxshift:
            maxshift = shift
            index = x
    if abs(a[m - 1] - a[m - 2]) > maxshift:
        maxshift = abs(a[m - 1] - a[m - 2])
        a[m - 1] = a[m - 2]
    else:
        a[index] = a[index + 1]
    turn = 0
    for x in xrange(1, m):
        turn += abs(a[x] - a[x - 1])
    return turn


n, m = map(int, raw_input().split())
a = map(int, raw_input().split())
print min_pages(n, m, a)
