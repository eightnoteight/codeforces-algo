#include <stdio.h>
long long int gcd (long long int a,long long int b )
{
  long long int c;
  while ( a != 0 ) {
     c = a; a = b%a;  b = c;
  }
  return b;
}
int main(int argc, const char *argv[])
{
    long long int l, r,i,j,k,vxy,vyz,vxz;
    scanf("%I64d%I64d", &l, &r);
    for (i = l; i <= r; i++) {
        for (j = i + 1; j <= r; j++) {
            for (k = j + 1; k <= r; k++) {
                vxy = gcd(i, j);
                vyz = gcd(j, k);
                vxz = gcd(i, k);
                if(vxy == 1 && vyz == 1 && vxz != 1){
                    printf("%I64d %I64d %I64d", i, j, k);
                    return 0;
                }
            }
        }
    }
    printf("-1");
    return 0;
}
