from fractions import gcd


def f():
    l, r = map(int, raw_input().split())
    for x in xrange(l, r + 1):
        for y in xrange(x + 1, r + 1):
            for z in xrange(y + 1, r + 1):
                varxy = gcd(x, y)
                varyz = gcd(y, z)
                varxz = gcd(x, z)
                if varxy == varyz == 1 and 1 != varxz:
                    print x, y, z
                    return
    print -1
f()
