c1, c2, p1, p2 = map(int, raw_input().split())
total = 0
x = 1
while total != c1 + c2:
    if (x % p1) and (x % p2):
        total += 1
    elif c1 > 0 and (x % p1):
        c1 -= 1
    elif c2 > 0 and (x % p2):
        c2 -= 1
    x += 1
print x - 1
