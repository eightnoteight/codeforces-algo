#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=C0111
'''input
2
0 0
0 3
'''


def main():
    mxa, mya = float('-inf'), float('-inf')
    mxi, myi = float('inf'), float('inf')
    for _ in xrange(int(raw_input())):
        x, y = map(int, raw_input().split())
        if mxa < x:
            mxa = x
        if mya < y:
            mya = y
        if mxi > x:
            mxi = x
        if myi > y:
            myi = y

    print max(mya - myi, mxa - mxi)**2


main()
