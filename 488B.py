
n = int(raw_input())
if n == 0:
    print("YES\n1\n2\n2\n3")
elif n == 1:
    t = int(raw_input())
    print("YES\n{}\n{}\n{}".format(2*t, 2*t, 3*t))
elif n == 2:
    x1 = int(raw_input())
    x2 = int(raw_input())
    mi = min(x1, x2)
    ma = max(x1, x2)
    if 3 * mi == ma:
        print("YES\n{}\n{}".format(4 * mi - 1, 1))
    else:
        print("YES\n{}\n{}".format(4 * ma - mi, 3 * ma))
elif n == 3:
    x1, x2, x3 = sorted([int(raw_input()), int(raw_input()), int(raw_input())])
    if 4 * x1 == x2 + x3:
        print("YES\n{}".format(3 * x1))
    elif 4 * x2 == x1 + x3:
        print("YES\n{}".format(3 * x2))
    elif 4 * x3 == x1 + x2:
        print("YES\n{}".format(3 * x3))

    elif 4 * x1 == 3 * (x2 + x3):
        print("YES\n{}".format(x1 // 4))
    elif 4 * x2 == 3 * (x1 + x3):
        print("YES\n{}".format(x2 // 4))
    elif 4 * x3 == 3 * (x1 + x2):
        print("YES\n{}".format(x3 // 4))

    elif 3 * x1 == x2 and x3 < 4 * x1:
        print("YES\n{}".format(4 * x1 - x3))
    elif 3 * x1 == x3 and x2 < 4 * x1:
        print("YES\n{}".format(4 * x1 - x2))
    elif 3 * x2 == x3 and x1 < 4 * x2:
        print("YES\n{}".format(4 * x2 - x1))
    else:
        print("NO")
else:
    x1 = int(raw_input())
    x2 = int(raw_input())
    x3 = int(raw_input())
    x4 = int(raw_input())
    if any([
        3 * x1 == x4 and 4 * x1 == x2 + x3,
        3 * x4 == x1 and 4 * x4 == x2 + x3,
        3 * x1 == x2 and 4 * x1 == x4 + x3,
        3 * x2 == x1 and 4 * x2 == x4 + x3,
        3 * x1 == x3 and 4 * x1 == x2 + x4,
        3 * x3 == x1 and 4 * x3 == x2 + x4,
        3 * x2 == x3 and 4 * x2 == x1 + x4,
        3 * x3 == x2 and 4 * x3 == x1 + x4,
        3 * x2 == x4 and 4 * x2 == x1 + x3,
        3 * x4 == x2 and 4 * x4 == x1 + x3,
        3 * x3 == x4 and 4 * x3 == x2 + x1,
        3 * x4 == x3 and 4 * x4 == x2 + x1
    ]):
        print("YES")
    else:
        print("NO")

