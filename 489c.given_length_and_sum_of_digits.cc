#include <bits/stdc++.h>
using namespace std;

int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    int m, s;
    cin >> m >> s;
    string minumber(m, '0');
    string manumber(m, '0');
    minumber[0] = manumber[0] = '1';
    if (s == 0 && m == 1) {
        cout << "0 0" << endl;
    }
    else if (s < 1 || 9*m < s) {
        cout << "-1 -1" << endl;
    }
    else {
        // minimum number construction
        int count = 1;
        auto miit = minumber.rbegin();
        while(count < s) {
            if (*miit == '9')
                miit++;
            ++*miit;
            ++count;
        }
        // maximum number construction
        count = 1;
        auto mait = manumber.begin();
        while(count < s) {
            if (*mait == '9')
                mait++;
            ++*mait;
            ++count;
        }
        cout << minumber << ' ' << manumber << endl;
    }
    return 0;
}
