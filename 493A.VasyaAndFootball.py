home = raw_input()
away = raw_input()
fouls = int(raw_input())
hteam = [0]*99
ateam = [0]*99
for foul in xrange(fouls):
    minute, ha, player_number, card = raw_input().split()
    player_number = int(player_number)
    if ha == 'h':
        if card == 'y':
            hteam[player_number] += 1
        else:
            hteam[player_number] += 2
            if hteam[player_number] == 3:
                print home, player_number, minute
        if hteam[player_number] == 2:
            print home, player_number, minute
    else:
        if card == 'y':
            ateam[player_number] += 1
        else:
            ateam[player_number] += 2
            if ateam[player_number] == 3:
                print away, player_number, minute
        if ateam[player_number] == 2:
            print away, player_number, minute
