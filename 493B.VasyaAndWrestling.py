l = [[], []]
s = [0, 0]
for i in xrange(int(raw_input())):
    ai = int(raw_input())
    l[ai < 0].append(abs(ai))
    s[ai < 0] += ai
    last = ai < 0

s[1] = -s[1]
n = len(l[0])
if s[0] == s[1]:
    i = 0
    while i < n and l[0][i] == l[1][i]:
        i += 1
    if i < len(l[0]):
        if l[0][i] < l[1][i]:
            print 'second'
        else:
            print 'first'
    else:
        if last:
            print 'second'
        else:
            print 'first'
elif s[0] > s[1]:
    print 'first'
else:
    print 'second'

