#!/usr/bin/env python
# -*- encoding: utf-8 -*-


n = int(raw_input())
first = map(int, raw_input().split())
m = int(raw_input())
second = map(int, raw_input().split())

total = []
for x in first:
    total.append((x, 1))

for x in second:
    total.append((x, 2))

total.sort(key=lambda x: x[0])
a = 3 * n
b = 3 * m
pa = -100000000000
pb = -100
for x in xrange(n + m):
    if pa - pb < a - b:
        pa = a
        pb = b
    elif (pa - pb == a - b) and (a > pa):
        pa = a
        pb = b
    if total[x][1] == 1:
        a -= 1
    else:
        b -= 1
if pa - pb < a - b:
    pa = a
    pb = b
elif (pa - pb == a - b) and (a > pa):
    pa = a
    pb = b

print '{}:{}'.format(pa, pb)

