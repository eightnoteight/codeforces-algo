#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=C0111


def main():
    a, b, c, d = map(int, raw_input().split())
    if max(3*a/10.0, a - (a/250.0)*c) < max(3*b/10.0, b - (b/250.0)*d):
        print 'Vasya'
    elif max(3*a/10.0, a - (a/250.0)*c) > max(3*b/10.0, b - (b/250.0)*d):
        print 'Misha'
    else:
        print 'Tie'



main()
