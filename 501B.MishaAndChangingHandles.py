#!/usr/bin/env python
# -*- encoding: utf-8 -*-

def main():
    users = {}
    backtrack = {}
    changed = set()
    for x in xrange(int(raw_input())):
        old, new = raw_input().split()
        if old in changed:
            users[backtrack[old]] = new
            backtrack[new] = backtrack[old]
            changed.add(new)
            del backtrack[old]
        else:
            users[old] = new
            backtrack[new] = old
            changed.add(new)
    print len(users)
    for oldest, newest in users.items():
        print oldest, newest


main()
