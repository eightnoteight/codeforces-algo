#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=C0111

def main():
    def observe(field, n, m, x, y):
        for irange in ((x, max(x - 2, -1), -1), (x, min(x + 2, n))):
            for jrange in ((y, max(y - 2, -1), -1), (y, min(y + 2, m))):
                fc = 0
                for i in xrange(*irange):
                    for j in xrange(*jrange):
                        if not field[i][j]:
                            fc += 1

                if fc == 4:
                    return True
        return False


    n, m, k = map(int, raw_input().split())
    field = [[True]*m for x in xrange(n)]
    for _ in xrange(k):
        x, y = map(int, raw_input().split())
        field[x - 1][y - 1] = False
        if observe(field, n, m, x - 1, y - 1):
            print _ + 1
            break
    else:
        print 0

main()

