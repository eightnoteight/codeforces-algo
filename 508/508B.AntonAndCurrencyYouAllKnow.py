#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=C0111

def main():
    n = list(raw_input().strip())
    maxswap = (0, -1)
    for x in xrange(len(n) - 2, -1, -1):
        if int(n[x]) % 2 == 0:
            if maxswap[0] == 0 or (int(n[-1]) - int(n[x]) > 0):
                maxswap = (int(n[-1]) - int(n[x]), x)

    if maxswap[1] != -1:
        x = maxswap[1]
        n[x], n[-1] = n[-1], n[x]
        print ''.join(n)
    else:
        print -1

main()
