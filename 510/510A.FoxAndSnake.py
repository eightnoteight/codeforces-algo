#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=C0111

# GleanStart
#9 9
# GleanEnd

def main():
    n, m = map(int, raw_input().split())
    a = '.' * (m - 1)
    b = '#'
    c = '#' * m
    for x in xrange(n):
        if x % 2 == 0:
            print c
        else:
            print "{}{}".format(a, b)
            a, b = b, a

main()

