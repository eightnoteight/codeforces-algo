#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=C0111
import array


class unionfind(object):
    def __init__(self, n):
        self._length = n
        self._roots = array.array('L', [x for x in xrange(n)])
        self._weights = array.array('L', [1]*n)

    def __str__(self):
        return str(self._roots)

    def union(self, a, b):
        assert isinstance(a, int) and isinstance(b, int)
        assert a < self._length and b < self._length

        aroot = self.find(a)
        broot = self.find(b)
        if self._weights[aroot] > self._weights[broot]:
            self._roots[broot] = aroot
            self._weights[aroot] += self._weights[broot]
            self._weights[broot] = 0
        else:
            self._roots[aroot] = broot
            self._weights[broot] += self._weights[aroot]
            self._weights[aroot] = 0

    def connected(self, a, b):
        assert isinstance(a, int) and isinstance(b, int)
        assert a < self._length and b < self._length

        return self.find(a) == self.find(b)

    def find(self, a):
        assert isinstance(a, int)

        while self._roots[a] != a:
            self._roots[a] = self._roots[self._roots[a]]
            a = self._roots[a]

        return a


# GleanStart
#2 13
#ABCDEFGHIJKLM
#NOPQRSTUVWXYZ
# GleanEnd

def main():
    def any_cycle(mat, n, m):
        uf = unionfind(n*m + m)
        for x in xrange(n):
            for y in xrange(m):
                if y != m - 1 and mat[x*m + y] == mat[x*m + y + 1]:
                    if uf.connected(x*m + y, x*m + y + 1):
                        return True
                    uf.union(x*m + y, x*m + y + 1)
                if x != n - 1 and mat[x*m + y] == mat[x*m + y + m]:
                    if uf.connected(x*m + y, x*m + y + m):
                        return True
                    uf.union(x*m + y, x*m + y + m)
        return False

    n, m = map(int, raw_input().split())
    mat = []
    for _ in xrange(n):
        mat.append(raw_input())
    mat = ''.join(mat)

    print 'Yes' if any_cycle(mat, n, m) else 'No'

main()

