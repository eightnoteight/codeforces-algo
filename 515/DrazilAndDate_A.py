#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,bad-builtin,missing-docstring
'''input
0 0 2
'''


def main():
    a, b, s = map(int, raw_input().split())
    if s >= abs(a) + abs(b) and (s - abs(a) - abs(b)) % 2 == 0:
        print 'Yes'
    else:
        print 'No'

main()
