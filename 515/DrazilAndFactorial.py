#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,bad-builtin,missing-docstring
'''input
3
555
'''


def main():
    raw_input()
    a = raw_input().strip()
    a = a.replace('1', '')
    a = a.replace('0', '')
    a = a.replace('4', '322')
    a = a.replace('6', '35')
    a = a.replace('8', '7222')
    a = a.replace('9', '7332')
    print ''.join(sorted(a, reverse=True))


main()
