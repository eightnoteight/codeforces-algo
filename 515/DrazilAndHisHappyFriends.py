#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,bad-builtin,missing-docstring
'''input
2 3
1 0
1 1
'''


def main():
    n, m = map(int, raw_input().split())
    boys = [False]*n
    girls = [False]*m
    for x in map(int, raw_input().split())[1: ]:
        boys[x] = True
    for x in map(int, raw_input().split())[1: ]:
        girls[x] = True
    for x in xrange(max(n*n*n, m*m*m) + 1):
        boys[x % n] = girls[x % m] = girls[x % m] or boys[x % n]

    if sum(boys) == n and sum(girls) == m:
        print 'Yes'
    else:
        print 'No'

main()
