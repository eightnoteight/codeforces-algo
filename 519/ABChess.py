#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from sys import stdin


def main():
    table = stdin.read().replace('\n', '').replace(' ', '')
    s = 0
    scores = {
        'Q': 9,
        'R': 5,
        'B': 3,
        'N': 3,
        'P': 1,
        'K': 0,
        '.': 0,
        'q': -9,
        'r': -5,
        'b': -3,
        'n': -3,
        'p': -1,
        'k': 0,
    }
    for x in table:
        s += scores[x]
    if s > 0:
        print 'White'
    elif s < 0:
        print 'Black'
    else:
        print 'Draw'


main()
