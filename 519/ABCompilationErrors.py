#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from sys import stdin
from collections import defaultdict


def main():
    inp = iter(map(int, stdin.read().split()))
    n = next(inp)
    a = defaultdict(int)
    for _ in xrange(n):
        a[next(inp)] += 2
    for _ in xrange(n - 1):
        a[next(inp)] -= 1
    for _ in xrange(n - 2):
        a[next(inp)] -= 1

    out = [0, 0]
    for x in a:
        if a[x] == 2:
            out[0] = x
        elif a[x] == 1:
            out[1] = x
        elif a[x] == 3:
            out = [x, x]
    print out[0]
    print out[1]


main()
