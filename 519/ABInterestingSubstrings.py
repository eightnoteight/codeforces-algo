#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin


def main():
    assignment = {
        letter: value for letter, value in zip(
            'abcdefghijklmnopqrstuvwxyz', map(int, raw_input().split()))
    }
    cache = {}
    p = 0
    s = raw_input()

    for char in s:
        p += assignment[char]
        try:
            cache[p][ord(char) - ord('a')] += 1
        except KeyError:
            cache[p] = [0]*26
            cache[p][ord(char) - ord('a')] += 1

    count = 0
    p = 0
    for char in s:
        p += assignment[char]
        try:
            cache[p][ord(char) - ord('a')] -= 1
            count += cache[p + assignment[char]][ord(char) - ord('a')]
        except KeyError:
            pass

    print count


main()