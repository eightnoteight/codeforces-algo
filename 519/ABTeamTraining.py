#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
'''input
2 6
'''
from sys import stdin


def main():
    n, m = map(int, stdin.read().split())
    c = float('-inf')
    for type1 in xrange(min(m // 2, n) + 1):
        c = max(c, type1 + min((n - type1) // 2, m - 2*type1))
    print c


main()
