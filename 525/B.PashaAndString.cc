#include <bits/stdc++.h>
using namespace std;

class SegmentTree {
 private:
    int _calc_size(int n) {
        int x = 1;
        while ((1 << x) < n)
            x++;
        return x;
    }
    void _rangeadd(int lo, int hi, int idx, int par) {
        if (lo >= hi)
            return;
        tree[idx] += par;
        if (ra_hi <= lo || ra_lo >= hi)
            return;
        if (ra_lo <= lo && hi <= ra_hi) {
            tree[idx] += prop_val;
            return;
        }
        int mid = lo + ((hi - lo) / 2);
        _rangeadd(lo, mid, 2*idx + 1, tree[idx]);
        _rangeadd(mid, hi, 2*idx + 2, tree[idx]);
        tree[idx] = 0;
    }
    void _push_prop(int lo, int hi, int idx, int par) {
        if (lo >= hi)
            return;
        if (hi - lo == 1)  {
            tree[idx] += par;
            arr[lo] = tree[idx];
            tree[idx] = 0;
            return;
        }
        int mid = lo + ((hi - lo) / 2);
        _push_prop(lo, mid, 2*idx + 1, tree[idx] + par);
        _push_prop(mid, hi, 2*idx + 2, tree[idx] + par);
        tree[idx] = 0;
    }
    vector<int> tree;
    int ra_lo;
    int prop_val;
    int ra_hi;

 public:
    vector<int> arr;
    SegmentTree(size_t n) {
        arr.resize(n);
        tree.resize((1 << (_calc_size(n) + 1)) + 1);
    }
    void rangeadd(int a, int b) {
        ra_lo = a;
        ra_hi = b;
        prop_val = 1;
        _rangeadd(0, arr.size(), 0, 0);
    }
    void push_prop() {
        _push_prop(0, arr.size(), 0, 0);
    }
};
int main(int argc, char const *argv[])
{
    char arr[200002];
    int m;
    scanf("%s %d", arr, &m);
    int n = strlen(arr);
    SegmentTree segt(n);
    for (int i = 0; i < m; ++i)
    {
        int a;
        scanf("%d", &a);
        segt.rangeadd(a - 1, n - a);
    }
    segt.push_prop();
    for (int i = 0; i < n / 2; ++i)
    {
        if (segt.arr[i] % 2)
        {
            swap(arr[i], arr[n - i - 1]);
        }
    }
    printf("%s\n", arr);
    return 0;
}
