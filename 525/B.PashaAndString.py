#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin

def main():
    s = list(raw_input())
    n = len(s)
    m = int(raw_input())
    arr = map(int, raw_input().split())
    arr.sort()
    k = 0
    for x in xrange(n // 2):
        while k < m and arr[k] - 1 <= x:
            k += 1
        if k % 2:
            s[x], s[n - x - 1] = s[n - x - 1], s[x]
    print ''.join(s)

main()
