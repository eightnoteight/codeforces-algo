#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from sys import stdin

def main():
    arr = map(int, stdin.read().split())
    arr.pop(0)
    arr.sort()
    arrf = iter(arr)
    s = 0
    for p, _, r, _ in zip(arrf, arrf, arrf, arrf):
        s += p*r
    print s

main()
