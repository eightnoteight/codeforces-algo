#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin


def play(a, b):
    ans = 0
    while b:
        ans += a // b
        a, b = b, a % b
    return ans


print play(*map(int, raw_input().split()))
