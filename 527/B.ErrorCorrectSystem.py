#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from sys import stdin


def hamming(n, S, T):
    n = int(n)
    dist = 0
    smark = {x:set() for x in 'abcdefghijklmnopqrstuvwxyz'}
    tmark = {x:set() for x in 'abcdefghijklmnopqrstuvwxyz'}
    for i, (x, y) in enumerate(zip(S, T)):
        if x != y:
            smark[x].add(y)
            tmark[y].add(x)
            dist += 1
    qswap = 0
    for ch in 'abcdefghijklmnopqrstuvwxyz':
        if ch in smark and ch in tmark:
            qswap = 1
            if smark[ch].intersection(tmark[ch]):
                qswap = 2
                break
    return dist - qswap



print hamming(*stdin.read().split())
