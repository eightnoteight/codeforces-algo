#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin

def main():
    luckyn = raw_input().strip().replace('4', '0').replace('7', '1')
    print int(luckyn, 2) + pow(2, len(luckyn)) - 1

main()

