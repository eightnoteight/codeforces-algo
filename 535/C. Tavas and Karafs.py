#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin

def main():
    a, b, n = map(int, raw_input().split())
    for _ in xrange(n):
        l, t, m = map(int, raw_input().split())
        A = b
        B = 2*a - b
        C = 2*t*m - 2*a*(1 - l) + b*(l - 1)*(l - 2)
        r = int(((B*B + 4*A*C)**0.5 - B) / (2*A))
        if a + (l - 1)*b <= t:
            print r
        else:
            print -1

main()
