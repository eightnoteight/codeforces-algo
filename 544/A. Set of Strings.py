#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from sys import stdin

def main():
    dstream = iter(stdin.read().split())
    k = int(next(dstream))
    s = next(dstream)
    if len(set(s)) < k:
        print 'NO'
        return
    print 'YES'
    arr = [[s[0]]]
    starts = set([s[0]])
    for x in xrange(1, len(s)):
        if len(arr) == k:
            for y in xrange(x, len(s)):
                arr[-1].append(s[y])
            break
        if s[x] not in starts:
            arr.append([s[x]])
            starts.add(s[x])
        else:
            arr[-1].append(s[x])

    print '\n'.join(map(''.join, arr))

main()
