#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from sys import stdin
from math import floor, ceil

def main():
    n, k = map(int, stdin.read().split())[:2]
    mat = [['S']*n for _ in xrange(n)]
    cc = 0
    for x in xrange(n):
        if cc == k:
            break
        if x % 2:
            for y in xrange(1, n, 2):
                mat[x][y] = 'L'
                if x > 0:
                    mat[x - 1][y] = 'S'
                if x < n - 1:
                    mat[x + 1][y] = 'S'
                if y > 0:
                    mat[x][y - 1] = 'S'
                if y < n - 1:
                    mat[x][y + 1] = 'S'
                cc += 1
                if cc == k:
                    break
        else:
            for y in xrange(0, n, 2):
                mat[x][y] = 'L'
                if x > 0:
                    mat[x - 1][y] = 'S'
                if x < n - 1:
                    mat[x + 1][y] = 'S'
                if y > 0:
                    mat[x][y - 1] = 'S'
                if y < n - 1:
                    mat[x][y + 1] = 'S'
                cc += 1
                if cc == k:
                    break
    if cc != k:
        print 'NO'
        return
    print 'YES'
    print '\n'.join(map(''.join, mat))


main()
