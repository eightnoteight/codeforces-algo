#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from sys import stdin
from itertools import imap

def main():
    dstream = imap(int, stdin.read().split())
    n = next(dstream)
    matrix = [[next(dstream) for _ in xrange(n)] for _ in xrange(n)]
    good = []
    for x in xrange(n):
        for y in xrange(n):
            if matrix[x][y] in [1, 3]:
                break
        else:
            good.append(x)
    print len(good)
    for x in good:
        print x + 1,

main()
