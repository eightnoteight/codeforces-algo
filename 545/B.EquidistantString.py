#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin

def main():
    s = raw_input().strip()
    t = raw_input().strip()
    p = bytearray()
    i, j = 0, 0
    for x, y in zip(s, t):
        if x != y:
            if i > j:
                j += 1
                p.append(x)
            else:
                i += 1
                p.append(y)
        else:
            p.append(x)
    if i == j:
        print p
        return
    p = bytearray()
    i, j = 0, 0
    for x, y in zip(s, t):
        if x != y:
            if i > j:
                j += 1
                p.append(x)
            else:
                i += 1
                p.append(y)
    if i == j:
        print p
    else:
        print 'impossible'


main()
