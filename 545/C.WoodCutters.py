#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from sys import stdin
from itertools import imap

def main():
    dstream = imap(int, stdin.read().split())
    n = next(dstream)
    arr = sorted(zip(dstream, dstream))
    dp = [((float('-inf'), 0), (float('-inf'), 0))]
    for i, (xi, hi) in enumerate(arr):
        # cutting part
        if xi - hi > dp[-1][0][0]:
            cut = (xi, max(dp[-1][0][1], dp[-1][1][1]) + 1)
        elif xi - hi > dp[-1][1][0]:
            cut = (xi, dp[-1][1][1] + 1)
            if (i == (n - 1) or xi + hi < arr[i + 1][0]) and cut[1] < (max(dp[-1][0][1], dp[-1][1][1]) + 1):
                cut = (xi + hi, max(dp[-1][0][1], dp[-1][1][1]) + 1)
        elif i == (n - 1) or xi + hi < arr[i + 1][0]:
            cut = (xi + hi, max(dp[-1][0][1], dp[-1][1][1]) + 1)
        else:
            cut = (xi, max(dp[-1][0][1], dp[-1][1][1]))

        uncut = (xi, max(dp[-1][0][1], dp[-1][1][1]))
        dp.append((cut, uncut))
    # print dp
    print max(dp[-1][0][1], dp[-1][1][1])

main()
