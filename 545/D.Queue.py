#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin

def main():
    n = int(raw_input())
    arr = sorted(map(int, raw_input().split()))
    sarr = [arr[0]]
    k = 1
    for x in xrange(1, n):
        if sarr[-1] <= arr[x]:
            sarr.append(sarr[-1] + arr[x])
            k += 1
    print k

main()
