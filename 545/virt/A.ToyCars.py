#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from sys import stdin
from itertools import imap

def main():
    dstream = imap(int, stdin.read().split())
    n = next(dstream)
    mat = [[next(dstream) for _ in xrange(n)] for _ in xrange(n)]
    goodCars = []
    for x in xrange(n):
        for y in xrange(n):
            if mat[x][y] in [1, 3]:
                break
        else:
            goodCars.append(x)
    print len(goodCars)
    for c in goodCars:
        print c + 1,

main()
