#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin

def main():
    s, t = raw_input(), raw_input()
    c, p = 0, bytearray()
    for x, y in zip(s, t):
        if x != y:
            c += 1
            p.append(x if c % 2 else y)
        else:
            p.append(x)
    if c % 2:
        print 'impossible'
    else:
        print p


main()
