#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from sys import stdin
from itertools import imap

def main():
    dstream = imap(int, stdin.read().split())
    n = next(dstream)
    arr = sorted(zip(dstream, dstream))
    count, last = 0, float('-inf')
    for i, (xi, hi) in enumerate(arr):
        if xi - hi > last:
            count += 1
            last = xi
        elif i == n - 1 or xi + hi < arr[i + 1][0]:
            count += 1
            last = xi + hi
        else:
            last = xi
    print count

main()
