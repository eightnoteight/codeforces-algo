#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin

def main():
    raw_input()
    ps, c = 0, 0
    for x in sorted(map(int, raw_input().split())):
        if ps <= x:
            ps += x
            c += 1
    print c

main()
