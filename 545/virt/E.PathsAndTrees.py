#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from sys import stdin
from itertools import imap
from collections import defaultdict
from heapq import heappush, heappop

def dijkstra(G, v, src):
    dist = [float('inf')]*v
    dist[src] = 0
    heap = [(dist[src], src)]
    while heap:
        node_dist, node = heappop(heap)
        if node_dist > dist[node]:
            continue
        dist[node] = node_dist
        for child, child_dist in G[node]:
            heappush(heap, (node_dist + child_dist, child))
    return dist

def main():
    dstream = imap(int, stdin.read().split())
    n, m = next(dstream), next(dstream)
    edgref, edges = {}, []
    graph = defaultdict(list)
    for i, x, y, w in zip(xrange(m), dstream, dstream, dstream):
        edgref[(x - 1, y - 1, w)] = i
        edges.append((x - 1, y - 1, w))
        graph[x - 1].append((y - 1, w))
        graph[y - 1].append((x - 1, w))
    u = next(dstream) - 1
    dist = dijkstra(graph, n, u)
    print dist
    k = 0
    tree = []
    for x, y, w in edges:
        if abs(dist[x] - dist[y]) != w:
            k += 1
        else:
            tree.append((x, y, w))
    print len(edges) - k
    for edg in tree:
        print edgref[edg],

main()
