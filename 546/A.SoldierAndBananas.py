#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin

def main():
    k, n, w = map(int, raw_input().split())
    res = ((w*(w + 1)) // 2)*k - n
    if res > 0:
        print res
    else:
        print 0

main()
