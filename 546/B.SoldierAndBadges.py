#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin

def main():
    raw_input()
    positions = [0]*6666
    arr = map(int, raw_input().split())
    for x in arr:
        positions[x] += 1
    # print positions[:10]
    cost = 0
    for x in xrange(6666):
        if positions[x] > 1:
            for y in xrange(x + 1, 6666):
                if positions[x] == 1:
                    break
                cost += 1*(positions[x] - 1)
                if positions[y] == 0:
                    positions[x] -= 1
                    positions[y] += 1
    # print positions[:10]
    print cost

main()
