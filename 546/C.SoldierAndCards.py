#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from collections import deque

def main():
    raw_input()
    pl1 = deque(reversed(map(int, raw_input().split())))
    pl1.pop()
    pl2 = deque(reversed(map(int, raw_input().split())))
    pl2.pop()
    visitedStates, steps = set(), 0
    while True:
        if not (pl1 and pl2):
            print steps, (1 if pl1 else 2)
            return
        if tuple(pl1) + tuple(pl2) in visitedStates:
            print -1
            return
        visitedStates.add(tuple(pl1) + tuple(pl2))
        x, y = pl1.pop(), pl2.pop()
        if x > y:
            pl1.appendleft(y)
            pl1.appendleft(x)
        else:
            pl2.appendleft(x)
            pl2.appendleft(y)
        steps += 1

main()


