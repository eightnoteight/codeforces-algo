#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin
from sys import stdin
from math import sqrt, ceil
from itertools import imap

def sieve(n):
    arr = [x for x in xrange(n)]
    for x in xrange(4, n, 2):
        arr[x] = 2
    for x in xrange(9, n, 6):
        arr[x] = 3
    for x in xrange(1, int(ceil((sqrt(n) + 1) / 6.0)) + 1):
        sxm1, sxp1 = 6*x - 1, 6*x + 1
        if arr[sxm1]:
            arr[sxm1*sxm1: n: 2*sxm1] = [sxm1]*int(ceil((n - sxm1*sxm1) / float(2*sxm1)))
        if arr[sxp1]:
            arr[sxp1*sxp1: n: 2*sxp1] = [sxp1]*int(ceil((n - sxp1*sxp1) / float(2*sxp1)))
    arr[1] = 0
    return arr

def pfactarr(n):
    primeFactors = [0]*n
    primesDivisors = sieve(n)
    for x in xrange(2, n):
        if primesDivisors[x] == x:
            primeFactors[x] = 1
        else:
            primeFactors[x] = primeFactors[x // primesDivisors[x]] + primeFactors[primesDivisors[x]]
    return primeFactors


def main():
    dstream = imap(int, stdin.read().split())
    arr = pfactarr(5000005)
    for x in xrange(1, 5000005):
        arr[x] += arr[x - 1]
    for _ in xrange(next(dstream)):
        a, b = next(dstream), next(dstream)
        print arr[a] - arr[b]


main()
