#!/usr/bin/python
# -*- encoding: utf-8 -*-
# pylint: disable=invalid-name,missing-docstring,bad-builtin


def main():
    s = raw_input().strip()
    k = int(raw_input())
    if len(s) % k:
        print 'NO'
    else:
        t = len(s) // k
        for x in xrange(k):
            x = s[t*x: t*x + t]
            if x != x[::-1]:
                print 'NO'
                return
        print 'YES'

main()
