#include <bits/stdc++.h>
using namespace std;

#ifndef __linux__
#define getc_unlocked getc
#define putc_unlocked putc

#endif
static struct IO {
    char tmp[1 << 10];
    char cur;
    inline char nextChar() {
        return cur = getc_unlocked(stdin);
    }
    inline char peekChar() {
        return cur;
    }
    inline operator bool() {
        return peekChar();
    }
    inline static bool isBlank(char c) {
        return (c < '-' && c > 0);
    }
    inline bool skipBlanks() {
        while (isBlank(nextChar()));
        return peekChar() > 0;
    }
    inline IO& operator >> (char & c) {
        c = nextChar();
        return *this;
    }
    inline IO& operator >> (char * buf) {
        if (skipBlanks()) {
            if (peekChar()) {
                *(buf++) = peekChar();
                while (!isBlank(nextChar()))
                    *(buf++) = peekChar();
            }
            *(buf++) = 0;
        }
        return *this;
    }
    inline IO& operator >> (string & s) {
        if (skipBlanks()) {
            s.clear();
            s += peekChar();
            while (!isBlank(nextChar()))
                s += peekChar();
        }
        return *this;
    }
    inline IO& operator >> (double & d) {
        if ((*this) >> tmp)
            sscanf(tmp, "%lf", &d);
        return *this;
    }
    #define defineInFor(intType) \
        inline IO& operator >>(intType & n) { \
            if (skipBlanks()) { \
                int sign = +1; \
                if (peekChar() == '-') { \
                    sign = -1; \
                    n = nextChar() - '0'; \
                } else \
                    n = peekChar() - '0'; \
                while (!isBlank(nextChar())) { \
                    n += n + (n << 3) + peekChar() - 48; \
                } \
                n *= sign; \
            } \
            return *this; \
        }
    defineInFor(int32_t)
    defineInFor(int64_t)
        inline void putChar(char c) {
            putc_unlocked(c, stdout);
        }
        inline IO& operator << (char c) {
            putChar(c);
            return *this;
        }
        inline IO& operator << (const char * s) {
            while (*s) putChar(*s++);
            return *this;
        }
        inline IO& operator << (const string & s) {
            for (int i = 0; i < (int)s.size(); ++i)
                putChar(s[i]);
            return *this;
        }
        char * toString(double d) {
            sprintf(tmp, "%lf%c", d, '\0');
            return tmp;
        }
        inline IO& operator << (double d) {
            return (*this) << toString(d);
        }
    #define defineOutFor(intType) \
        inline char * toString(intType n) { \
            char * p = (tmp + 30); \
            if (n) { \
                bool isNeg = 0; \
                if (n < 0) isNeg = 1, n = -n; \
                while (n) \
                    *--p = (n % 10) + '0', n /= 10; \
                if (isNeg) *--p = '-'; \
            } else *--p = '0'; \
            return p; \
        } \
        inline IO& operator << (intType n) { return (*this) << toString(n); }
    defineOutFor(int32_t)
    defineOutFor(int64_t)
    #define endl ('\n')
    #define cout __io__
    #define cin __io__

    // dict output
    template<class T, class U>
    inline IO& operator << (map<T, U>& p) {
        (*this) << "{";
        for (typename map<T, U>::iterator it = p.begin(); it != p.end(); it++) {
            (*this) << it->first << ": " << it->second << ", ";
        }
        (*this) << "}";
        return *this;
    }

    // pair input
    template<class T, class U>
    inline IO& operator >> (pair<T, U>& p) {
        (*this) >> p.first >> p.second;
        return *this;
    }
    // pair output
    template<class T, class U>
    inline IO& operator << (pair<T, U>& p) {
        (*this) << '(' << p.first << ", " << p.second << ')';
        return *this;
    }
    // pair input
    template<class T, class U>
    inline IO& operator >> (const pair<T, U>& p) {
        (*this) >> p.first >> p.second;
        return *this;
    }
    // pair output
    template<class T, class U>
    inline IO& operator << (const pair<T, U>& p) {
        (*this) << '(' << p.first << ", " << p.second << ')';
        return *this;
    }
    // vector input
    template<class T>
    inline IO& operator >> (vector<T>& d) {
        for (size_t i = 0; i < d.size(); ++i)
            (*this) >> d[i];
        return *this;
    }
    // vector output
    template<class T>
    inline IO& operator << (vector<T>& d) {
        (*this) << "[";
        for (size_t i = 0; i < d.size(); ++i)
            (*this) << d[i] << ", ";
        (*this) << "]";
        return *this;
    }

} __io__;

class maxelem
{
public:
    int32_t* arr;
    vector<int32_t> tree;
    int32_t size;
    int32_t upidx;
    int32_t upval;
    int32_t qulo;
    int32_t quhi;
    maxelem(int32_t* array, int32_t n) {
        arr = array;
        tree.resize((2*(1 << (int32_t)ceil(log2(n)))) + 1);
        construct(0, n, 0);
        size = n;
    }
    void construct(int32_t lo, int32_t hi, int32_t idx) {
        if (hi - lo == 1) {
            tree[idx] = lo;
            return;
        }
        int32_t mid = lo + ((hi - lo) / 2);
        construct(lo, mid, 2*idx + 1);
        construct(mid, hi, 2*idx + 2);
        tree[idx] = ((arr[tree[2*idx + 1]] > arr[tree[2*idx + 2]]) ? (tree[2*idx + 1]) : (tree[2*idx + 2]));
    }
    void update_util(int32_t lo, int32_t hi, int32_t idx) {
        if (hi - lo == 1) {
            if (lo == upidx) {
                arr[upidx] = upval;
                tree[idx] = upidx;
            }
            return;
        }
        int32_t mid = lo + ((hi - lo) / 2);
        if (upidx < mid)
            update_util(lo, mid, 2*idx + 1);
        else
            update_util(mid, hi, 2*idx + 2);
        tree[idx] = (arr[tree[2*idx + 1]] > arr[tree[2*idx + 2]])? (tree[2*idx + 1]) : (tree[2*idx + 2]);
    }
    void update(int32_t idx, int32_t nval) {
        upidx = idx;
        upval = nval;
        update_util(0, size, 0);
    }
    int32_t find_util(int32_t lo, int32_t hi, int32_t idx) {
        if (quhi <= lo or qulo >= hi or lo >= hi) {
            return -1;
        }
        if (qulo <= lo and hi <= quhi) {
            return tree[idx];
        }
        int32_t mid = lo + ((hi - lo) / 2);
        int32_t left = find_util(lo, mid, 2*idx + 1);
        int32_t right = find_util(mid, hi, 2*idx + 2);
        if (left == -1)
            return right;
        if (right == -1)
            return left;
        return ((arr[left] > arr[right]) ? (left): (right));
    }
    int32_t find(int32_t qlo, int32_t qhi) {
        qulo = qlo;
        quhi = qhi;
        return find_util(0, size, 0);
    }
};


int main() {
    int n, m, q, i, j;
    cin >> n >> m >> q;
    int32_t grid[n][m];
    int32_t gridsums[n];
    for (int x = 0; x < n; ++x) {
        gridsums[x] = 0;
        int consec = 0;
        for (int y = 0; y < m; ++y) {
            cin >> grid[x][y];
            if (y && grid[x][y] && grid[x][y] == grid[x][y - 1]) {
                consec++;
            }
            else {
                gridsums[x] = max(consec, gridsums[x]);
                consec = grid[x][y];
            }
            gridsums[x] = max(consec, gridsums[x]);
        }
    }
    maxelem segt(gridsums, n);
    while(q--) {
        cin >> i >> j;
        i--; j--;
        grid[i][j] = 1 - grid[i][j];
        int consec = 0;
        gridsums[i] = 0;
        for (int x = 0; x < m; ++x) {
            if (x && grid[i][x] && grid[i][x] == grid[i][x - 1]) {
                consec++;
            }
            else {
                gridsums[i] = max(consec, gridsums[i]);
                consec = grid[i][x];
            }
            gridsums[i] = max(consec, gridsums[i]);
        }
        segt.update(i, gridsums[i]);
        cout << gridsums[segt.find(0, n)] << endl;
    }
    return 0;
}
