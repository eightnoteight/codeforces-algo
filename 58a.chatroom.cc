#include <bits/stdc++.h>
using namespace std;

int main() {
    ios::sync_with_stdio(0);cin.tie(0);
    string word;
    cin >> word;
    const char* hello = "hello";
    for (char c: word)
        if (c == *hello)
            hello++;
    cout << (*hello?"NO":"YES");
    return 0;
}
