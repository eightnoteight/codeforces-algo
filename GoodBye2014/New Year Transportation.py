from sys import stdin


def main():
    l = map(int, stdin.read().split())
    i = 0
    while i < l[0] and i + l[i + 2] < l[1]:
        i = i + l[i + 2]
    print 'YES' if i == l[1] - 1 else 'NO'
main()
