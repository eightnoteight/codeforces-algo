#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pylint: disable=C0111
'''input
2
'''


def nextnum(num):
    n = len(num)
    for x in xrange(n - 2, -1, -1):
        dig = num[x]
        miy = (chr(255), chr(255))
        for y in xrange(x + 1, n):
            if miy > (num[y], y):
                miy = (num[y], y)
        if miy > (dig, x):
            num[x], num[miy[1]] = num[miy[1]], num[x]
            return num[:x + 1] + sorted(num[x + 1:])
    return 'Impossible'


def main():
    print ''.join(nextnum(list(raw_input())))


main()
